package ro.tuc.pt.assign4.bll;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import java.util.Observable;
import ro.tuc.pt.assign4.data.FileWriter;
import ro.tuc.pt.assign4.data.RestaurantSerializator;

public class Restaurant extends Observable implements RestaurantProcessing, Serializable{

	private static final long serialVersionUID = 4513696897096361588L;
	private List<MenuItem> menuItems;
	private List<Order> orderList;
	private Map<Order, List<MenuItem>> orders;
	private int nextProductID;
	private int nextOrderID;
	

	@SuppressWarnings("unchecked")
	public Restaurant(){
		menuItems = new ArrayList<>();
		orderList = new ArrayList<>();
		orders = new HashMap<>();
		RestaurantSerializator serializator = new RestaurantSerializator();
		menuItems = (List<MenuItem>) serializator.deserialize();
		MenuItem lastItem = menuItems.get(menuItems.size()-1);
		if(BaseProduct.class.isInstance(lastItem)){
			nextProductID = ((BaseProduct)lastItem).getId();
		} else {
			nextProductID = ((CompositeProduct)lastItem).getId();
		}
	}

	@Override
	public boolean addMenuItem(String name, String description, float price) {
		//Start Precondditions
		assert(name != null && price > 0 && description!=null);
		int oldSize =  menuItems.size();
		//End Precondditions
		nextProductID++;
		MenuItem item = new BaseProduct(nextProductID,  name, description, price);
		menuItems.add(item);
		//Start Postconditions + invariants
		 assert(menuItems.size() == oldSize + 1);
		 assert(isWellFormed());
		//End Postconditions + invariants
		return true;
	}

	@Override
	public boolean composeItems(String name, String description, List<Integer> idList) {
		//Start Precondditions
		assert(name != null && idList.size() > 0 && description != null);
		int oldSize =  menuItems.size();
		//End Precondditions
		nextProductID++;
		MenuItem item = new CompositeProduct(nextProductID, name, description);
		for(Integer currentId : idList){
			Iterator<MenuItem> itr = menuItems.iterator();
			while(itr.hasNext()) {
				MenuItem currentItem = itr.next();
				if(BaseProduct.class.isInstance(currentItem)){
					if(((BaseProduct)currentItem).getId() == currentId){
						((CompositeProduct)item).addItem(currentItem);
					}
				}
			}
		}
		menuItems.add(item);
		//Start Postconditions + invariants
		assert(menuItems.size() == oldSize + 1);
		assert(isWellFormed());
		//End Postconditions + invariants
		return true;
	}

	@Override
	public boolean editMenuItem(int id, String name, String description, float price) {
		//Start Precondditions
		assert(name!= null && price >0 && description != null && id>-1);
		int oldSize =  menuItems.size();
		//End Precondditions
		Iterator<MenuItem> itr = menuItems.iterator();
		boolean found = false;
        while(itr.hasNext() && !found){
        	MenuItem item = itr.next();
			if(BaseProduct.class.isInstance(item)){
				if(((BaseProduct)item).getId() == id){
					((BaseProduct)item).setName(name);
					((BaseProduct)item).setDescription(description);
					((BaseProduct)item).setPrice(price);
					found = true;
				}
			}
		}
		//Postconditions + invariants
		assert(menuItems.size() == oldSize);
		assert(isWellFormed());
        //End Postconditions + invariants
		return true;
	}
	
	@Override
	public boolean editCompositeItem(int id, String name, String description, List<Integer> idList) {
		//Precondditions
		assert(name!= null && description != null && id>-1 && idList.size()>0);
		int oldSize =  menuItems.size();
		//End Precondditions
		Iterator<MenuItem> itr = menuItems.iterator();
		boolean found = false;
        while(itr.hasNext() && !found){
        	MenuItem item = itr.next();
			if(CompositeProduct.class.isInstance(item)){
				if(((CompositeProduct)item).getId() == id){
					((CompositeProduct)item).setName(name);
					((CompositeProduct)item).setDescription(description);
					Iterator<MenuItem> iterator = menuItems.iterator();
					List<MenuItem> itemList = new ArrayList<>();
					 while(iterator.hasNext()){
						 MenuItem addedItem = iterator.next();
						 if(BaseProduct.class.isInstance(addedItem)){
							 if(idList.contains(((BaseProduct)addedItem).getId())){
								 itemList.add((BaseProduct)addedItem);
							 }
						 }
					 }
					((CompositeProduct)item).setItems(itemList);
					found = true;
				}
			}
		}
		//Postconditions + invariants
		assert(menuItems.size() == oldSize);
		assert(isWellFormed());
        //End Postconditions + invariants
		return true;
	}
	
	private boolean isWellFormed() {
		return true;
	}

	private void removeFromComposite(MenuItem item){
		Iterator<MenuItem> itr = menuItems.iterator();
		while(itr.hasNext()){
			MenuItem currentItem = itr.next();
			if(CompositeProduct.class.isInstance(currentItem)){
				((CompositeProduct)currentItem).removeItem(item);
			}
		}
	}
	
	@Override
	public boolean deleteMenuItem(int id) {
		//Precondditions
		assert(id>-1);
		int oldSize =  menuItems.size();
		//End Precondditions
        Iterator<MenuItem> itr = menuItems.iterator();
		boolean found = false;
        while(itr.hasNext() && !found){
        	MenuItem item = itr.next();
			if(BaseProduct.class.isInstance(item)){
				if(((BaseProduct)item).getId() == id){
					removeFromComposite(item);
					found = true;
					itr.remove();
				}
			} else {
				if(((CompositeProduct)item).getId() == id){
					found = true;
					itr.remove();
				}
			}
		}
		//Postconditions + invariants
		assert(menuItems.size() == oldSize - 1);
		assert(isWellFormed());
        //End Postconditions + invariants
		return true;
	}

	@Override
	public boolean createNewOrder(int tableNo, List<Integer> idList) {
		//Precondditions
		assert(tableNo>0 && idList.size()>0);
		int oldSize =  orderList.size();
		//End Precondditions
		Order order = new Order(nextOrderID, tableNo);
		orderList.add(order);
		List<MenuItem> itemList = new ArrayList<>();
		Iterator<MenuItem> itr;
		MenuItem item;
		boolean found;
		for(Integer id : idList){
			found = false;
			itr = menuItems.iterator();
			while(itr.hasNext() && !found) {
				item = itr.next();
				if(BaseProduct.class.isInstance(item)){
					if(((BaseProduct)item).getId() == id){
						itemList.add(item);
					}
				} else {
					if(((CompositeProduct)item).getId() == id){
						itemList.add(item);
					}
				}
			}
		}
		orders.put(order, itemList);
        setChanged();
        notifyObservers(itemList);
		nextOrderID++;
		//Postconditions + invariants
		assert(orderList.size() == oldSize + 1);
		assert(isWellFormed());
        //End Postconditions + invariants
		return true;
	}

	@Override
	public float computePriceOrder(int orderID) {
		//Precondditions
		assert(orderID > -1);
		//End Precondditions
		Order currentOrder = orderList.get(orderID);
		List<MenuItem> items = orders.get(currentOrder);
		float price = 0;
		for(MenuItem item : items){
			price += item.computePrice(); 
		}
		//Postconditions + invariants
		assert(price > 0);
		assert(isWellFormed());
        //End Postconditions + invariants
		return price;
	}
	
	@Override
	public boolean generateBill(int orderID) {
		//Precondditions
		assert(orderID > -1);
		//End Precondditions
		Order order = orderList.get(orderID);
		FileWriter writer = new FileWriter();
		writer.writeBill(order, orders.get(order), this.computePriceOrder(orderID));
		//Postconditions + invariants
		assert(isWellFormed());
        //End Postconditions + invariants 
		return true;
	}
	
	public void closeRestaurant(){
		RestaurantSerializator serializator = new RestaurantSerializator();
		serializator.serialize(menuItems);
	}
	
	public List<MenuItem> getItems(){
		return menuItems;
	}
	
	public List<Order> getOrders(){
		return orderList;
	}
	
	public List<MenuItem> getOrder(Order order){
		return orders.get(order);
	}
	
	public MenuItem getItem(int itemID) {
		Iterator<MenuItem> itr = menuItems.iterator();
		MenuItem item = null;
		boolean found = false;
        while(itr.hasNext() && !found){
        	item = itr.next();
			if(CompositeProduct.class.isInstance(item)){
				if(((CompositeProduct)item).getId() == itemID){
					return item;
				}
			} else {
				if(((BaseProduct)item).getId() == itemID){
					return item;
				}
			}
		}
        return null;
	}
	
	public static void main(String[] args){
		
		Restaurant rest = new Restaurant();
		/*rest.addMenuItem("Cereals", "I's yummy", 2.7f);
		rest.addMenuItem("Eggs", "Boiled eggs", 8.7f);
		rest.addMenuItem("Bacon", "Smells amzing", 5.7f);
		rest.addMenuItem("Milk", "Cold milk", 4.7f);
		List<Integer> idList = new ArrayList<>();
		idList.add(0);
		idList.add(1);
		rest.composeItems("Basic lunch", "cheap bastard", idList);
		idList.add(2);
		idList.add(3);
		rest.composeItems("Expensive lunch", "Pretentious preak", idList);
		System.out.println(rest.getItems());
		rest.editMenuItem(0, "Just cereals", "No milk", 2f);
		System.out.println(rest.getItems());
		idList.remove(1);
		idList.remove(2);
		rest.editCompositeItem(5, "Less Expensive Lunch", "I like it", idList);
		System.out.println(rest.getItems());
		rest.deleteMenuItem(1);
		rest.deleteMenuItem(3);

		rest.addMenuItem("Bacon2", "Smells amzing", 5.7f);
		System.out.println("\n\n\nHere:\n");
		System.out.println(rest.getItems());
		
		
		//Waiter actions
		List<Integer> orderItems = new ArrayList<>();
		orderItems.add(4);
		orderItems.add(1);
		rest.createNewOrder(4, orderItems);
		orderItems.add(3);
		rest.createNewOrder(3, orderItems);
		List<Order> ord = rest.getOrders();
		System.out.println(rest.getOrder(ord.get(0)));
		System.out.println("Next: ");
		System.out.println(rest.getOrder(ord.get(1)));
		
		System.out.println("Price : " + rest.computePriceOrder(0));
		System.out.println("Price : " + rest.computePriceOrder(1));

		rest.generateBill(0);
		
		rest.closeRestaurant();
		*/
		System.out.println(rest.getItems());
		//System.out.println(rest.getOrders());
		List<Integer> idList = new ArrayList<>();
		idList.add(1);
		idList.add(5);
		rest.createNewOrder(10, idList);
		System.out.println("\n\nHere:\n" + rest.getOrders().get(0));
		System.out.println(rest.getOrder(rest.getOrders().get(0)));
		//System.out.println(rest.getItems());
	}

}
