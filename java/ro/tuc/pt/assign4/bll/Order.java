package ro.tuc.pt.assign4.bll;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Order implements Serializable{
	private static final long serialVersionUID = 6241506373280523482L;
	private int orderID;
	private Date date;
	private int tableNo;

	public Order(int orderID, int tableNo) {
		this.orderID = orderID;
		this.date = new Date(System.currentTimeMillis());
		this.tableNo = tableNo;
	}

	public int getOrderID() {
		return orderID;
	}

	public Date getDate() {
		return date;
	}

	public int getTableNo() {
		return tableNo;
	}

	public void setTableNo(int tableNo) {
		this.tableNo = tableNo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj.getClass() != this.getClass()){
			return false;
		}
		if(obj.hashCode() != this.hashCode()){
			return false;
		}
		if(this.orderID != ((Order)obj).getOrderID()){
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(orderID, date);
	}

	public String toString() {
		return "Order [id = " + orderID + ", date = " + date.toString() + ", tableNo = " + tableNo + "]\r\n";
	}
}
