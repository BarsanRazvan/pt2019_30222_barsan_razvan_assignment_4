package ro.tuc.pt.assign4.bll;

public interface MenuItem{
	public float computePrice();
}
