package ro.tuc.pt.assign4.bll;

import java.io.Serializable;

public class BaseProduct implements MenuItem, Serializable{
	private static final long serialVersionUID = -3332491629325748538L;
	private int id;
	private float price;
	private String name;
	private String description;

	public BaseProduct(int id, String name, String description, float price){
		this.id = id;
		this.price = price;
		this.name = name;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public float computePrice() {
		return price;
	}
	
	@Override
	public String toString(){
		return "Item [id = " + id + ", name = " + name + ", description = " + description +
				", price = " + price + "]\r\n";
	}
}
