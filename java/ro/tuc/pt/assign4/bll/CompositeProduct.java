package ro.tuc.pt.assign4.bll;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompositeProduct implements MenuItem, Serializable {

	private static final long serialVersionUID = 3754741717992716701L;
	private int id;
	private String name;
	private String description;
	private List<MenuItem> items;

	
	public CompositeProduct(int id, String name, String description){
		this.id = id;
		this.name = name;
		this.description = description;
		items = new ArrayList<>();
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void addItem(MenuItem item){
		items.add(item);
	}
	
	public void removeItem(MenuItem item){
		items.remove(item);
	}
	
	public List<MenuItem> getItems(){
		return items;
	}
	
	@Override
	public float computePrice() {
		float price = 0;
		for(MenuItem item : items){
			price += item.computePrice();
		}
		return price;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setItems(List<MenuItem> items) {
		this.items = items;
	}
	
	@Override
	public String toString(){
		String string = "Item [id = " + id + " name = " + name + ", price = " + this.computePrice()
						+ ", containing: ";
		for(MenuItem item : items){
			string += item.toString() + " ";
		}
		string += "\r\n";
		return string;
	}
}
