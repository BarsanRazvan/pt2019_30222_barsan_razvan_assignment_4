package ro.tuc.pt.assign4.bll;

import java.util.List;

public interface RestaurantProcessing {

	/**
	 * 
	 * @pre name!= null && price >0 && description!= null
	 * @post menuItems.size() == oldSize + 1
	 * @inv isWellFormed()
	 */
	public boolean addMenuItem(String name, String description, float price);
	/**
	 * 
	 * @pre name!= null && idList.size()>0 && description != null
	 * @post menuItems.size() == oldSize + 1
	 * @inv isWellFormed()
	 */
	public boolean composeItems(String name, String description, List<Integer> idList);
	/**
	 * 
	 * @pre name!= null && price >0 && description != null && id>-1 && idList.size()>0
	 * @post menuItems.size() == oldSize
	 * @inv isWellFormed()
	 */
	public boolean editMenuItem(int id, String name, String description, float price);
	/**
	 * 
	 * @pre name!= null && description != null && id>-1 && idList.size()>0
	 * @post menuItems.size() == oldSize
	 * @inv isWellFormed()
	 */
	public boolean editCompositeItem(int id, String name, String description, List<Integer> idList);
	
	/**
	 * 
	 * @pre id>-1
	 * @post menuItems.size() == oldSize -1
	 * @inv isWellFormed()
	 */
	public boolean deleteMenuItem(int id);
	
	/**
	 * 
	 * @pre tableNo>0 && idList.size()>0
	 * @post orderList.size() == oldSize -1
	 * @inv isWellFormed()
	 */
	public boolean createNewOrder(int tableNo, List<Integer> idList);
	/**
	 * 
	 * @pre orderID > -1
	 * @post price > 0
	 * @inv isWellFormed()
	 */
	public float computePriceOrder(int orderID);
	/**
	 * 
	 * @pre orderID > -1
	 * @post no change
	 * @inv isWellFormed()
	 */
	public boolean generateBill(int orderID);
}
