package ro.tuc.pt.assign4.presentation;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

public class WaiterGraphicalUserInterface extends JFrame{
	
	private static final long serialVersionUID = 9080383668751372553L;
	private JTable baseProductsTable;
	private JTable compositeProductsTable;
	private JTable ordersTable;
	private JLabel singleProductLabel;
	private JLabel menuProductLabel;
	private JLabel ordersLabel;
	private JLabel tableNoLabel;
	private JButton createOrder;
	private JButton computePrice;
	private JButton generateBill;
	private JComboBox<Integer> tableNoBox;
	private JPanel upPanel;
	private JPanel downPanel;
	private JScrollPane scrollBase;
	private JScrollPane scrollComposed;
	private JScrollPane scrollOrders;
	private JPanel titlePanel;
	
	public WaiterGraphicalUserInterface(){
		init();
		setTitlePanel();
		setDownPanel();
		setUpPanel();
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.add(titlePanel);
		this.add(upPanel);
		this.add(Box.createRigidArea(new Dimension(100,0)));
		this.add(downPanel);
		this.setLocationRelativeTo(null);
		this.setTitle("Waiter");
		this.setSize(1280,1024);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	private void setDownPanel() {
		downPanel.setLayout(new BoxLayout(downPanel, BoxLayout.X_AXIS));
		downPanel.add(tableNoLabel);
		downPanel.add(Box.createHorizontalGlue());
		downPanel.add(tableNoBox);
		downPanel.add(Box.createHorizontalGlue());
		downPanel.add(createOrder);
		downPanel.add(Box.createRigidArea(new Dimension(700,0)));
		downPanel.add(computePrice);
		downPanel.add(Box.createHorizontalGlue());
		downPanel.add(generateBill);
		downPanel.setMaximumSize(new Dimension(1500, 30));
	}
	
	private void setUpPanel() {
		scrollComposed.setPreferredSize(new Dimension(400,400));
		upPanel.setLayout(new BoxLayout(upPanel, BoxLayout.X_AXIS));
		upPanel.add(scrollBase);
		upPanel.add(scrollComposed);
		upPanel.add(Box.createHorizontalGlue());
		upPanel.add(scrollOrders);
	}
	
	private void setTitlePanel() {
		titlePanel.add(singleProductLabel);
		titlePanel.add(Box.createRigidArea(new Dimension(500,0)));
		titlePanel.add(menuProductLabel);
		titlePanel.add(Box.createRigidArea(new Dimension(600,0)));
		titlePanel.add(ordersLabel);
		titlePanel.setMaximumSize(new Dimension(1500, 30));
	}
	
	private void init(){
		titlePanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();
		baseProductsTable = new JTable();
		compositeProductsTable = new JTable();
		ordersTable = new JTable();
		singleProductLabel = new JLabel("Single Product");
		menuProductLabel = new JLabel("Composite Product");
		ordersLabel = new JLabel("Orders");
		tableNoLabel = new JLabel("Tabel No: ");
		tableNoBox = new JComboBox<>();
		createOrder = new JButton("Create Order");
		computePrice = new JButton("Compute Price");
		generateBill = new JButton("Generate Bill");
		scrollBase = new JScrollPane(baseProductsTable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollComposed = new JScrollPane(compositeProductsTable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollOrders = new JScrollPane(ordersTable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		for(int i = 1; i < 11; i++)
			tableNoBox.addItem(i);
	}
	
	public void updateOrderTable(TableModel model){
		ordersTable.setModel(model);
		
	}
	
	public void setBaseProductTable(TableModel model){
		baseProductsTable.setModel(model);
	}
	
	public void setCompositeProductTable(TableModel model){
		compositeProductsTable.setModel(model);
	}
	
	public void addCreateOrderListener(ActionListener ac) {
		createOrder.addActionListener(ac);
	}
	
	public void addComputePriceListener(ActionListener ac) {
		computePrice.addActionListener(ac);
	}
	
	public void addGenerateBillListener(ActionListener ac) {
		generateBill.addActionListener(ac);
	}
	
	public void addWindowClose(WindowAdapter wa) {
		this.addWindowListener(wa);
	}
	
	public void showDialogBox(String title, String message) {
		JOptionPane.showMessageDialog(this, message, title,  JOptionPane.PLAIN_MESSAGE);
	}
	
	public int getTableNo() {
		return (Integer)tableNoBox.getSelectedItem();
	}
		
	public int getSelectedOrder() {
		int[] order = ordersTable.getSelectedRows();
		return Integer.parseInt(ordersTable.getValueAt(order[0], 0).toString());
	}
	
	public List<Integer> getOrderItems(){
		List<Integer> orderItems = new ArrayList<>();
	    int[] baseItems = baseProductsTable.getSelectedRows();
	    int[] compositeItems = compositeProductsTable.getSelectedRows();
	    for(int item : baseItems) {
	    	orderItems.add(Integer.parseInt(baseProductsTable.getValueAt(item, 0).toString()));
	    }
	    for(int item : compositeItems) {
	    	orderItems.add(Integer.parseInt(compositeProductsTable.getValueAt(item, 0).toString()));
	    }
		return orderItems;
	}
}
