package ro.tuc.pt.assign4.presentation;


import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import ro.tuc.pt.assign4.bll.BaseProduct;
import ro.tuc.pt.assign4.bll.CompositeProduct;
import ro.tuc.pt.assign4.bll.MenuItem;

public class ChefGraphicalUserInterface extends JFrame implements Observer{

	private static final long serialVersionUID = 6964838700560064691L;
	private JTable ordersTable;
	private JPanel downPanel;
	private JPanel titlePanel;
	private JButton finalizeButton;
	private JScrollPane scrollOrders;

	
	public ChefGraphicalUserInterface() {
		init();
		setTitlePanel();
		setDownPanel();
		setOrderTable();
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.add(titlePanel);
		this.add(scrollOrders);
		this.add(Box.createRigidArea(new Dimension(100,0)));
		this.add(downPanel);
		this.setLocationRelativeTo(null);
		this.setTitle("Chef");
		this.setSize(1280,1024);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	private void init(){
		titlePanel = new JPanel();
		downPanel = new JPanel();
		ordersTable = new JTable();
		finalizeButton = new JButton("Finalize Order");
		scrollOrders = new JScrollPane(ordersTable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	}
	
	public void addWindowClose(WindowAdapter wa) {
		this.addWindowListener(wa);
	}
	
	private void setDownPanel() {
		downPanel.setLayout(new BoxLayout(downPanel, BoxLayout.X_AXIS));
		downPanel.add(Box.createRigidArea(new Dimension(100,0)));
		downPanel.add(finalizeButton);
	}
	
	private void setTitlePanel() {
		titlePanel.setMaximumSize(new Dimension(1500, 30));
		titlePanel.add(new JLabel("Orders"));
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("here");
		@SuppressWarnings("unchecked")
		List<MenuItem> itemList = (List<MenuItem>)arg1;
		TableModel model = ordersTable.getModel();
		String items = "";
		int index = 0;
		for(MenuItem item : itemList) {
			if(BaseProduct.class.isInstance(item)){
				items += "Item " + index + " is " + ((BaseProduct)item).getName() + " |";
			} else {
				items += "Item " + index + " is " + ((CompositeProduct)item).getName() + " | ";
			}
		}
		((DefaultTableModel) model).addRow(new Object[]{items});
	}
	
	public void setOrderTable(){
		String[] columns = {"items"};
		Object[][] rows = {};
		TableModel model = new DefaultTableModel(rows, columns) {
			private static final long serialVersionUID = -113661017554557870L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return column != 0;
			}
		};
		ordersTable.setModel(model);
	}
}
