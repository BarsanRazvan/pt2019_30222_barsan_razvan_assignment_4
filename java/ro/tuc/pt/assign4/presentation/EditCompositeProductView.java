package ro.tuc.pt.assign4.presentation;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class EditCompositeProductView extends JFrame{
	private static final long serialVersionUID = 1712880873754183304L;
	private JTable baseProductTable;
	private JTable currentProductTable;
	private JPanel downPanel;
	private JPanel upPanel;
	private JPanel titlePanel;
	private JTextField nameTF;
	private JTextField descriptionTF;
	private JButton saveButton;
	private JButton addButton;
	private JButton deleteButton;
	private JScrollPane scrollBaseProduct;
	private JScrollPane scrollCurrentBaseProduct;

	
	public EditCompositeProductView() {
		init();
		setTitlePanel();
		setUpPanel();
		setDownPanel();
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.add(titlePanel);
		this.add(upPanel);
		this.add(Box.createRigidArea(new Dimension(100,0)));
		this.add(downPanel);
		this.setLocationRelativeTo(null);
		this.setTitle("Edit composite product");
		this.setSize(800,600);
		this.setVisible(true);
	}
	
	private void init(){
		titlePanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();
		baseProductTable = new JTable();
		currentProductTable = new JTable();
		nameTF = new JTextField(20);
		descriptionTF = new JTextField(20);
		saveButton = new JButton("Save Changes");
		deleteButton = new JButton("Delete Product");
		addButton = new JButton("Add products");
		scrollBaseProduct = new JScrollPane(baseProductTable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollCurrentBaseProduct = new JScrollPane(currentProductTable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	}
	
	private void setUpPanel() {
		upPanel.setLayout(new BoxLayout(upPanel, BoxLayout.X_AXIS));
		upPanel.add(scrollBaseProduct);
		upPanel.add(scrollCurrentBaseProduct);
	}
	
	private void setDownPanel() {
		downPanel.setLayout(new BoxLayout(downPanel, BoxLayout.X_AXIS));
		downPanel.setMaximumSize(new Dimension(1500, 30));
		downPanel.add(new JLabel("Name :"));
		downPanel.add(nameTF);
		downPanel.add(new JLabel("Description :"));
		downPanel.add(descriptionTF);
		downPanel.add(addButton);
		downPanel.add(deleteButton);
		downPanel.add(saveButton);
	}
	
	private void setTitlePanel() {
		titlePanel.setMaximumSize(new Dimension(1500, 30));
		titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.X_AXIS));
		titlePanel.add(Box.createRigidArea(new Dimension(200,0)));
		titlePanel.add(new JLabel("All Products"));
		titlePanel.add(Box.createRigidArea(new Dimension(300,0)));
		titlePanel.add(new JLabel("Current Products"));
	}
	
	public void updateCurrentProductTable(TableModel model){
		currentProductTable.setModel(model);
	}
	
	public void setBaseProductsTable(TableModel model){
		baseProductTable.setModel(model);
	}
	
	public void addProductListener(ActionListener ac) {
		addButton.addActionListener(ac);
	}
	
	public void deleteProductListener(ActionListener ac) {
		deleteButton.addActionListener(ac);
	}
	
	public void saveProductListener(ActionListener ac) {
		saveButton.addActionListener(ac);
	}
	
	public String getNameTF() {
		return nameTF.getText();
	}
	
	public String getDescriptionTF() {
		return descriptionTF.getText();
	}
	
	public void showDialogBox(String title, String message) {
		JOptionPane.showMessageDialog(this, message, title,  JOptionPane.PLAIN_MESSAGE);
	}
	
	public void addBaseItemRow(String id, String price, String name, String description) {
		DefaultTableModel model = (DefaultTableModel) currentProductTable.getModel();
		model.addRow(new Object[] { id, price, name, description });
		currentProductTable.setModel(model);
	}
	
	public List<Integer> getSelectedBaseIds(){
		int[] selectedIndex = baseProductTable.getSelectedRows();
		List<Integer> baseItem = new ArrayList<>();
		try {
			for(int index : selectedIndex)
				baseItem.add(Integer.parseInt(baseProductTable.getValueAt(index, 0).toString()));
		}
		catch(ArrayIndexOutOfBoundsException ex) {
			this.showDialogBox("Error!", "Please, select a product!");
		}
		return baseItem;
	}
	
	public void removeSelectedRows() {
		DefaultTableModel model = (DefaultTableModel) currentProductTable.getModel();
		int [] ids = currentProductTable.getSelectedRows();
		int nrRemoved = 0;
		for(int id : ids) {
			model.removeRow(id - nrRemoved);
			nrRemoved++;
		}
		currentProductTable.setModel(model);
	}
	
	public List<Integer> getNewBaseIds(){
		DefaultTableModel model = (DefaultTableModel) currentProductTable.getModel();
		List<Integer> baseItem = new ArrayList<>();
		for (int count = 0; count < model.getRowCount(); count++){
			  baseItem.add(Integer.parseInt(model.getValueAt(count, 0).toString()));
		}
		return baseItem;
	}
	
	public void closeWindow() {
		this.dispose();
	}
	
}
