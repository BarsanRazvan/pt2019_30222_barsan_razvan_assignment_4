package ro.tuc.pt.assign4.presentation;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class AdministratorGraphicalUserInterface extends JFrame{

	private static final long serialVersionUID = 3706685007821468120L;
	private JTable baseProductsTable;
	private JTable compositeProductsTable;
	private JLabel singleProductLabel;
	private JLabel menuProductLabel;
	private JPanel upPanel;
	private JPanel downPanel;
	private JPanel addPanel;
	private JButton addButton;
	private JButton editBaseProductButton;
	private JButton deleteBaseProductButton;
	private JButton composeNewItemButton;
	private JButton editCompositeProductButton;
	private JButton deleteCompositeProductButton;
	private JTextField nameTF;
	private JTextField descriptionTF;
	private JTextField priceTF;
	private JScrollPane scrollBase;
	private JScrollPane scrollComposed;
	private JPanel titlePanel;
	
	public AdministratorGraphicalUserInterface() {
		init();
		setTitlePanel();
		setAddPanel();
		setUpPanel();
		setDownPanel();
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.add(titlePanel);
		this.add(upPanel);
		this.add(Box.createRigidArea(new Dimension(100,0)));
		this.add(downPanel);
		this.setLocationRelativeTo(null);
		this.setTitle("Administrator");
		this.setSize(1280,1024);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	private void init(){
		titlePanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();
		addPanel = new JPanel();
		baseProductsTable = new JTable();
		compositeProductsTable = new JTable();
		singleProductLabel = new JLabel("Single Product");
		menuProductLabel = new JLabel("Composite Product");
		nameTF = new JTextField(20);
		descriptionTF = new JTextField(20);
		priceTF = new JTextField(20);
		addButton = new JButton("Add Product");
		editBaseProductButton = new JButton("Edit Base Product");
		deleteBaseProductButton = new JButton("Delete Base Product");
		composeNewItemButton = new JButton("Compose Product");
		editCompositeProductButton = new JButton("Edit Composed Product");
		deleteCompositeProductButton = new JButton("Delete Composed Product");
		scrollBase = new JScrollPane(baseProductsTable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollComposed = new JScrollPane(compositeProductsTable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	}
	
	private void setUpPanel() {
		scrollComposed.setPreferredSize(new Dimension(400,400));
		upPanel.setLayout(new BoxLayout(upPanel, BoxLayout.X_AXIS));
		upPanel.add(scrollBase);
		upPanel.add(scrollComposed);
		upPanel.add(addPanel);
	}
		
	private void setTitlePanel() {
		titlePanel.add(singleProductLabel);
		titlePanel.add(Box.createRigidArea(new Dimension(700,0)));
		titlePanel.add(menuProductLabel);
		titlePanel.setMaximumSize(new Dimension(1500, 30));
	}
	
	private void setAddPanel() {
		addPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		addPanel.setLayout(new BoxLayout(addPanel, BoxLayout.Y_AXIS));
		addPanel.setMaximumSize(new Dimension(600, 300));
		nameTF.setMaximumSize(new Dimension(600, 30));
		descriptionTF.setMaximumSize(new Dimension(600, 30));
		priceTF.setMaximumSize(new Dimension(600, 30));
		addButton.setMaximumSize(new Dimension(600, 30));
		addPanel.add(new JLabel("Name : "));
		addPanel.add(nameTF);
		addPanel.add(Box.createRigidArea(new Dimension(0,10)));
		addPanel.add(new JLabel("Description : "));
		addPanel.add(descriptionTF);
		addPanel.add(Box.createRigidArea(new Dimension(0,10)));
		addPanel.add(new JLabel("Price : "));
		addPanel.add(priceTF);
		addPanel.add(Box.createRigidArea(new Dimension(0,10)));
		addPanel.add(addButton);
	}
	
	private void setDownPanel() {
		downPanel.setLayout(new BoxLayout(downPanel, BoxLayout.X_AXIS));
		downPanel.add(editBaseProductButton);
		downPanel.add(Box.createHorizontalGlue());
		downPanel.add(deleteBaseProductButton);
		downPanel.add(Box.createHorizontalGlue());
		downPanel.add(composeNewItemButton);
		downPanel.add(Box.createHorizontalGlue());
		downPanel.add(editCompositeProductButton);
		downPanel.add(Box.createHorizontalGlue());
		downPanel.add(deleteCompositeProductButton);
		downPanel.setMaximumSize(new Dimension(1500, 30));
	}
	
	public String getNewProductName() {
		return nameTF.getText();
	}
	
	public String getNewProductDescription() {
		return descriptionTF.getText();
	}
	
	public float getNewProductPrice() {
		return Float.parseFloat(priceTF.getText());
	}
	
	public void setBaseProductTable(TableModel model){
		baseProductsTable.setModel(model);
	}

	public void setCompositeProductTable(TableModel model){
		compositeProductsTable.setModel(model);
	}
	
	public void addBaseProductListener(ActionListener ac) {
		addButton.addActionListener(ac);
	}
	
	public void editBaseProductListener(ActionListener ac) {
		editBaseProductButton.addActionListener(ac);
	}
	
	public void deleteBaseProductListener(ActionListener ac) {
		deleteBaseProductButton.addActionListener(ac);
	}
	
	public void composeNewItemListener(ActionListener ac) {
		composeNewItemButton.addActionListener(ac);
	}
	
	public void editCompositeProductListener(ActionListener ac) {
		editCompositeProductButton.addActionListener(ac);
	}
	
	public void deleteCompositeProductListener(ActionListener ac) {
		deleteCompositeProductButton.addActionListener(ac);
	}
	
	public void addWindowClose(WindowAdapter wa) {
		this.addWindowListener(wa);
	}
	
	public List<String> getSelectedBaseItem(){
		int[] selectedIndex = baseProductsTable.getSelectedRows();
		List<String> baseItem = new ArrayList<>();
		try {
			baseItem.add(baseProductsTable.getValueAt(selectedIndex[0], 0).toString());
			baseItem.add(baseProductsTable.getValueAt(selectedIndex[0], 1).toString());
			baseItem.add(baseProductsTable.getValueAt(selectedIndex[0], 2).toString());
			baseItem.add(baseProductsTable.getValueAt(selectedIndex[0], 3).toString());
		}
		catch(ArrayIndexOutOfBoundsException ex) {
			this.showDialogBox("Error!", "Please, select a product!");
		}
		return baseItem;
	}
	
	public List<Integer> getSelectedBaseIds(){
		int[] selectedIndex = baseProductsTable.getSelectedRows();
		List<Integer> baseItem = new ArrayList<>();
		try {
			for(int index : selectedIndex)
				baseItem.add(Integer.parseInt(baseProductsTable.getValueAt(index, 0).toString()));
		}
		catch(ArrayIndexOutOfBoundsException ex) {
			this.showDialogBox("Error!", "Please, select a product!");
		}
		return baseItem;
	}
	
	public DefaultTableModel getBaseProductModel() {
		return (DefaultTableModel) baseProductsTable.getModel();
	}
	
	//To do:
	public List<String> getSelectedCompositeItem(){
		int[] selectedIndex = compositeProductsTable.getSelectedRows();
		List<String> compositeItem = new ArrayList<>();
		try {
			compositeItem.add(compositeProductsTable.getValueAt(selectedIndex[0], 0).toString());
			return compositeItem;
		}
		catch(ArrayIndexOutOfBoundsException ex) {
			this.showDialogBox("Error!", "Please, select a product!");
		}
		return compositeItem;
	}
	
	public void showDialogBox(String title, String message) {
		JOptionPane.showMessageDialog(this, message, title,  JOptionPane.PLAIN_MESSAGE);
	}
	
}
