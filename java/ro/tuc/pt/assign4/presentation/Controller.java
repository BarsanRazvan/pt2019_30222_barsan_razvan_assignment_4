package ro.tuc.pt.assign4.presentation;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import ro.tuc.pt.assign4.bll.BaseProduct;
import ro.tuc.pt.assign4.bll.CompositeProduct;
import ro.tuc.pt.assign4.bll.MenuItem;
import ro.tuc.pt.assign4.bll.Restaurant;

public class Controller {
	private Restaurant restaurant;
	private WaiterGraphicalUserInterface waiter;
	private AdministratorGraphicalUserInterface admin;
	private ChefGraphicalUserInterface chef;

	public Controller() {
		restaurant = new Restaurant();
		waiter = new WaiterGraphicalUserInterface();
		admin = new AdministratorGraphicalUserInterface();
		chef = new ChefGraphicalUserInterface();
		restaurant.addObserver(chef);
		updateMenuItemsTables();
		addAdministratorListeners();
		addWaiterListeners();
		addExitListeners();
	}

	private String[] createHeader(List<Object> objects) {
		ArrayList<String> columns = new ArrayList<>();
		Field[] fields = objects.get(0).getClass().getDeclaredFields();
		for (Field field : fields) {
			if (field != fields[0])
				columns.add(field.getName());
		}
		String[] header = new String[columns.size()];
		header = columns.toArray(header);
		return header;
	}

	private Object[][] createData(List<Object> objects) {
		Field[] fields = objects.get(0).getClass().getDeclaredFields();
		ArrayList<Object[]> rows = new ArrayList<>();
		Object value;
		for (Object obj : objects) {
			ArrayList<String> rowEntry = new ArrayList<>();
			for (Field field : fields) {
				if (field != fields[0]) {
					field.setAccessible(true);
					try {
						value = field.get(obj);
						rowEntry.add(value.toString());
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			}
			rows.add(rowEntry.toArray());
		}
		Object[][] data = new Object[rows.size()][rows.get(0).length];
		data = rows.toArray(data);
		return data;
	}

	public TableModel createTable(List<Object> objects) {
		if (objects.size() == 0) {
			return new DefaultTableModel();
		}
		String[] columns = createHeader(objects);
		Object[][] rows = createData(objects);
		return new DefaultTableModel(rows, columns) {
			private static final long serialVersionUID = -2700868953498791061L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return column != 0;
			}
		};
	}

	private void addWaiterListeners() {
		waiter.addCreateOrderListener(ev -> {
			List<Integer> idList = waiter.getOrderItems();
			int tableNo = waiter.getTableNo();
			restaurant.createNewOrder(tableNo, idList);
			updateOrderTable();
		});

		waiter.addComputePriceListener(ev -> {
			int orderID = waiter.getSelectedOrder();
			float computedPrice = restaurant.computePriceOrder(orderID);
			waiter.showDialogBox("Computed price", "The price for the order " + orderID + " is " + computedPrice);
		});

		waiter.addGenerateBillListener(ev -> {
			int orderID = waiter.getSelectedOrder();
			restaurant.generateBill(orderID);
			waiter.showDialogBox("Generated bill", "Bill generated successfully!");
		});
	}
	
	private void addExitListeners() {
		WindowAdapter adapter = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
    			restaurant.closeRestaurant();
                System.exit(0);
            }
        };
		chef.addWindowClose(adapter);
		admin.addWindowClose(adapter);
		waiter.addWindowClose(adapter);
	}

	private void addAdministratorListeners() {
		admin.addBaseProductListener(ev -> {
			restaurant.addMenuItem(admin.getNewProductName(), admin.getNewProductDescription(),
					admin.getNewProductPrice());
			updateMenuItemsTables();
			admin.showDialogBox("Add", "Product added successfully!");
		});

		admin.editBaseProductListener(ev -> {
			List<String> editedItem = admin.getSelectedBaseItem();
			int id = Integer.parseInt(editedItem.get(0));
			float price = Float.parseFloat(editedItem.get(1));
			restaurant.editMenuItem(id, editedItem.get(2), editedItem.get(3), price);
			updateMenuItemsTables();
			admin.showDialogBox("Edit", "Product edited successfully!");
		});

		admin.deleteBaseProductListener(ev -> {
			List<String> editedItem = admin.getSelectedBaseItem();
			restaurant.deleteMenuItem(Integer.parseInt(editedItem.get(0)));
			updateMenuItemsTables();
			admin.showDialogBox("Delete", "Product deleted successfully!");
		});

		admin.composeNewItemListener(ev -> {
			List<Integer> idList = admin.getSelectedBaseIds();
			restaurant.composeItems(admin.getNewProductName(), admin.getNewProductDescription(), idList);
			updateMenuItemsTables();
			admin.showDialogBox("Composed", "Product composed successfully!");
		});

		admin.editCompositeProductListener(ev -> {
			EditCompositeProductView edit = new EditCompositeProductView();
			edit.setBaseProductsTable(admin.getBaseProductModel());
			int compositeID = Integer.parseInt(admin.getSelectedCompositeItem().get(0));
			CompositeProduct product = (CompositeProduct) restaurant.getItem(compositeID);
			edit.updateCurrentProductTable(createTable(new ArrayList<Object>(product.getItems())));
			addEditListeners(edit, product);
		});

		admin.deleteCompositeProductListener(ev -> {
			List<String> editedItem = admin.getSelectedCompositeItem();
			restaurant.deleteMenuItem(Integer.parseInt(editedItem.get(0)));
			updateMenuItemsTables();
			admin.showDialogBox("Delete", "Product deleted successfully!");
		});
	}

	private void addEditListeners(EditCompositeProductView edit, CompositeProduct product) {
		edit.addProductListener(ev -> {
			List<Integer> idList = edit.getSelectedBaseIds();
			for (Integer id : idList) {
				BaseProduct selectedProduct = (BaseProduct) restaurant.getItem(id);
				edit.addBaseItemRow(selectedProduct.getId() + "", selectedProduct.computePrice() + "",
						selectedProduct.getName(), selectedProduct.getDescription());
			}
		});
		
		edit.deleteProductListener(ev -> {
			edit.removeSelectedRows();
		});
		
		edit.saveProductListener(ev -> {
			List<Integer> idList = edit.getNewBaseIds();
			String name;
			String description;
			if(edit.getNameTF().equals(""))
				name = product.getName();
			else name = edit.getNameTF();
			if(edit.getDescriptionTF().equals(""))
				description = product.getDescription();
			else description = edit.getDescriptionTF();
			System.out.println("This: " + idList);
			restaurant.editCompositeItem(product.getId(), name, description, idList);
			updateMenuItemsTables();
			admin.showDialogBox("Composed", "Product edited successfully!");
			edit.closeWindow();
		});
	}

	private void updateOrderTable() {
		List<Object> orders = new ArrayList<>(restaurant.getOrders());
		waiter.updateOrderTable(createTable(orders));
	}

	private void updateMenuItemsTables() {
		List<Object> objects = new ArrayList<>();
		for (MenuItem item : restaurant.getItems()) {
			if (BaseProduct.class.isInstance(item))
				objects.add(item);
		}
		waiter.setBaseProductTable(createTable(objects));
		admin.setBaseProductTable(createTable(objects));
		objects.clear();
		for (MenuItem item : restaurant.getItems()) {
			if (CompositeProduct.class.isInstance(item))
				objects.add(item);
		}
		waiter.setCompositeProductTable(createTable(objects));
		admin.setCompositeProductTable(createTable(objects));
	}

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		Controller cont = new Controller();
	}
}
