package ro.tuc.pt.assign4.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class RestaurantSerializator {

	public RestaurantSerializator() {

	}

	public void serialize(Object restaurant) {
		try {
			FileOutputStream fileOut = new FileOutputStream(System.getProperty("user.dir")
					+ "\\src\\main\\java\\ro\\tuc\\pt\\assign4\\serialization\\items.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(restaurant);
			out.close();
			fileOut.close();
			System.out.printf("Serialization completed with no errors!");
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	public Object deserialize() {
		try {
			FileInputStream fileIn = new FileInputStream(System.getProperty("user.dir")
					+ "\\src\\main\\java\\ro\\tuc\\pt\\assign4\\serialization\\items.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			Object returnedObject = in.readObject();
			in.close();
			fileIn.close();
			return returnedObject;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

}
