package ro.tuc.pt.assign4.data;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import ro.tuc.pt.assign4.bll.BaseProduct;
import ro.tuc.pt.assign4.bll.CompositeProduct;
import ro.tuc.pt.assign4.bll.MenuItem;
import ro.tuc.pt.assign4.bll.Order;

public class FileWriter {
	
	public FileWriter(){
		
	}
	
	public void writeBill(Order order, List<MenuItem> items, float price) {
		PrintWriter writer; 
		String path = System.getProperty("user.dir") + "\\src\\main\\java\\ro\\tuc\\pt\\assign4\\bills\\";
		try {
			writer = new PrintWriter(path + order.getOrderID() + ".txt", "UTF-8");
			writer.println(order.toString());
			for(MenuItem item : items){
				if(CompositeProduct.class.isInstance(item))
					writer.println(((CompositeProduct)item).toString());
				else writer.println(((BaseProduct)item).toString());
			}
			writer.println("Total: " + price);
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
